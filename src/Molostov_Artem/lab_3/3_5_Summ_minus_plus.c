/*�������� ���������, ������� ��������� ������������� ������ ������� N, �
����� ������� ����� ���������, ������������� ����� ������ ����������-
��� � ��������� ������������� ����������.
���������:
������ ����������� ���������� �������: �������������� � ������������-
�� ������� (��� ����� �������...)
*/

#include <stdlib.h>
#include <stdio.h>
#define N 50 // ����� �������

int main()
{    
    unsigned short i,start,finish,found=0;
    int str[N],summ=0;
    srand(time(0));
    for(i=0;i<N;i++)
    {
        str[i]= rand()%201+(-100); // ����� ���������� �����
        if (str[i]<0 && found==0) // �������� �� ����
        {
            start=i; // ���� �������������, �� ����� ������
            found=1; // ������ = 1
        }
        if (str[i]>0) // ���� �������������, �� ����� ���������
        finish=i; // �� ���������� ��������������.
        printf("%d ",str[i]); // ������ �������
    }
    putchar('\n');
    printf("First negative = %d, last positive = %d",str[start],str[finish]);
    putchar('\n');
    for(i=start+1;i<finish;i++) // ������� �����
    summ=summ+str[i];
    printf("Summ = %d",summ); // ����� �����
    putchar('\n');
return 0;
}