/*
�������� ���������, ������������ � ������ ���������� ������������������
������������ �����
���������:
��� ������ AABCCCDDEEEEF ��������� 4 � EEEE
*/
#include <stdio.h>
#include <string.h>
#define N 100 // ����� ������

int main()
{
    char str[N]; // ������ ��� ������
    char i=0; // �������
    unsigned short max=0; // ������������ ���������� ����������
    unsigned short count_l=1; // ������� ���������� ����������
    unsigned short start=0,finish=0; // ������ � ����� ��� ������
    unsigned short j; // ���������� ��� ������ ������������������.

    puts("Enter a line:"); //���� ������
    fgets(str,N,stdin);
    if(str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=' ';
    while(str[i])
    {
        if(str[i]==str[i-1]) count_l++; // �������� ����������
        else 
        {
            if (count_l>=max) // �������� � ���������� �����������
            {
                max=count_l; // ���� ������, �� ���������� ������������
                finish=i; // ����������� ����� ������
                start=i-count_l; // ����������� ������ ������
                count_l=1; // "���������" ������������������
            }
        }
    i++;
    }
    if (max==1) printf("There is no repeats!\n"); // ����� ���� �� ���� ����������
    else
    {
        printf("'");
        for (j=start;j<finish;j++) printf("%c",str[j]); // ������ ����� �� ������� �����������
        printf("' - %d",max); // ����� ��� �����
        putchar('\n');
    }
    return 0;
}