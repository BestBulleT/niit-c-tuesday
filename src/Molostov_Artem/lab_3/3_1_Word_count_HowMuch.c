/*
�������� ���������, �������������� ���������� ���� �� �����-
��� ������������� ������
*/
#include <stdio.h>
#include <string.h>
#define N 100 // ����� ������

int main()
{
    char str[N]; // ������ ��� ������
    char inWord=0; // ������ "� �����"
    char i=0; // �������
    unsigned short count=0; // ������� ����

    puts("Enter a line:");
    fgets(str,N,stdin);
    if(str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=' ';
    while(str[i])
    {
        if(str[i]!=' ' && inWord==0) // ������ �����
            inWord=1;
        else if(str[i]==' ' && inWord==1) // ������ ������
        {
            inWord=0;
            count++; // ������� ���� +1
        }
        i++; // ��������� ������� �� ��������
    }
    printf("Word number is %u\n",count); // ����� ���������� ����
    return 0;
}