/*
�������� ���������, ������� ����������� ������� ����� � ���-
���� ��:��:��, � ����� ������� ����������� � ����������� ��
���������� ������� ("������ ���� "������ ����"� �.�.)
*/

#include <stdio.h>

void clean_stdin()
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c !=EOF);
}

int main()
{
   int hour,minute,second; // ���������� ��� ����� �����, �����, ������
    while(1)
   {
       printf("What time is now [format: HH:MM:SS]\n"); // �������� ������������ ����� ������� �������� ������� � ���������������
       if ((scanf("%d:%d:%d",&hour,&minute,&second)==3)  && 
           (hour<24 && hour>=0 && minute>=0 && minute<60 && second<60 && second>=0))
                break;
            else 
                printf("Input error, try again use [format: HH:MM:SS]\n");
       clean_stdin();
    }
    puts(hour>=0 && hour<6?"Good Night user":(hour>=6 && hour<12)?"Good Morning user":(hour>=12 && hour<18)?"Good Afternoon user":"Good Evening user");
        // �������� ����� 0-6=����, 6-12=����, 12-18=����, 18-24=���� � ���������� ����� �����������
    return 0;
}