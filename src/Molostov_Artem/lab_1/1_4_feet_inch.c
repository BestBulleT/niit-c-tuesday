/*�������� ���������, ������� ��������� ���� �� ������������ ��-
����� (����, �����) � ����������� (����������). ������ ���-
����� � ���� ���� ����� �����, ��������� � ���� �������������
����� � ��������� �� 1 �����. 1 ��� = 12 ������. 1 ���� = 2.54
��.
*/

#include <stdio.h>

void clean_stdin()
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c !=EOF);
}

int main()
{
  int feet,inch; // ���������� ��� ����� ����� � ������
  float result; // ���������� ��� �������� ����������
  while(1) // �������� ������������ ����� ������
   {
       puts("Enter your American Height");
        if ((scanf("%d %d",&feet,&inch)==2) && ((inch<12 && inch>=0) && (feet>=0))) // ���������� ������ �� ������ ���� ������ 12
                break; 
            else 
            puts("Input error [Feet] <Space> [Inch < 12] positive");
            clean_stdin();
   }
   result=(feet*30.48)+(inch*2.54); // ������� � ���������� 1 ���=30.48 ��, 1 ����=2.54 ��
   printf("Your American height %d Feet %d Inch = %.1f European SM\n",feet,inch,result); // ����� ���������� � ��������� �� 1 �����
   return 0;
}